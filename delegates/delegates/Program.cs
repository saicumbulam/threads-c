﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

delegate void BinaryOperation(int x, int y);

namespace delegates
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("[{0}] Main called",Thread.CurrentThread.ManagedThreadId);
#if (false)
            Add(2, 2);
#else
            BinaryOperation asyncAdd = Add;
            asyncAdd.BeginInvoke(2, 2, null, null);
#endif
            Console.WriteLine("[{0}] Main done",Thread.CurrentThread.ManagedThreadId);
        }

        static void Add(int a, int b)
        {
            Console.WriteLine("[{0}] Add ({1},{2}) => {3}",Thread.CurrentThread.ManagedThreadId,
                a,b,(a+b));
        }
    }
}
