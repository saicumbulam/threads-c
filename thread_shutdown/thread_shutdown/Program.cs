﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace thread_shutdown
{
    class Program
    {
        static volatile bool cancel = false;
        static void Main(string[] args)
        {
            Thread t = new Thread(SayHello);
            t.Start();

            Console.WriteLine("Press Enter to cancel");
            Console.ReadLine();

            cancel = true;
            t.Join();

            Console.WriteLine("Done");
        }
        static void SayHello()
        {
            while (!cancel)
            {
                Console.WriteLine( "Hello, world");
                Thread.Sleep(1000);
            }
        }
    }
}
