﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace queue_user_workitem
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("[{0}] Main called",Thread.CurrentThread.ManagedThreadId);
            for (int n = 0; n < 10; n++)
            {
                ThreadPool.QueueUserWorkItem(sayHello, n);
            }
            Thread.Sleep(rng.Next(1000, 3000));
            Console.WriteLine("[{0}] Main done", Thread.CurrentThread.ManagedThreadId);
        }
        static Random rng = new Random();
        static void sayHello(object arg)
        {
            Thread.Sleep(rng.Next(250, 500));
            int n = (int)arg;
            Console.WriteLine("[{0}] Hello, World {1}! {2}",Thread.CurrentThread.ManagedThreadId,
                n,Thread.CurrentThread.IsBackground);
        }
    }
}
