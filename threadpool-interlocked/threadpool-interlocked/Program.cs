﻿using System;
using System.Threading;

namespace threadpool_interlocked
{
    internal class Program
    {
        private static int totalValue = 0;
        private const int MinimumClrThreads = 3;   //use this value to control the threads
        private const int MaximumClrThreads = 4;   //use this value to control the threads

        private static void Main(string[] args)
        {
            int workerThreads;
            int portThreads;
            // Queue the task.
            Tuple<int, int> threadpool1 = InitThreadPool();
            ThreadPool.SetMinThreads(threadpool1.Item1, threadpool1.Item2);
            ThreadPool.QueueUserWorkItem(ThreadProc);
            Console.WriteLine("Main thread does some work, then sleeps.");
            Thread.Sleep(1000);
            ThreadPool.GetAvailableThreads(out workerThreads, out portThreads);
            Console.WriteLine("\nMaximum worker threads: \t{0}" +
    "\nMaximum completion port threads: {1}",
    workerThreads, portThreads);
            Console.WriteLine("Main thread exits.");
        }

        // This thread procedure performs the task.
        private static void ThreadProc(Object stateInfo)
        {
            // No state object was passed to QueueUserWorkItem, so stateInfo is null.
            Console.WriteLine("Hello from the thread pool.");
            //Program totalValue = new Program();
            Interlocked.Increment(ref totalValue);
            Console.WriteLine("sum value:" + totalValue);
        }

        private static Tuple<int, int> InitThreadPool()
        {
            int logicalProcessors = Environment.ProcessorCount;

            int minWorkerThreads = MinimumClrThreads * logicalProcessors;
            int minIoThreads = MinimumClrThreads * logicalProcessors;
            //int maxWorkerThreads = MaximumClrThreads * logicalProcessors;
            //int maxIoThreads = MaximumClrThreads * logicalProcessors;

            //set the minumum number of worker and IO threads
            //ThreadPool.SetMaxThreads(maxWorkerThreads, maxIoThreads);
            //ThreadPool.SetMinThreads(minWorkerThreads, minIoThreads);
            return Tuple.Create(minWorkerThreads, minIoThreads);
        }

        // The example displays output like the following:
        //       Main thread does some work, then sleeps.
        //       Hello from the thread pool.
        //       Main thread exits.
    }
}