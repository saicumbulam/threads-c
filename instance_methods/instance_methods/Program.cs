﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace instance_methods
{
    class Program
    {
        static void Main(string[] args)
        {
            Messenger m = new Messenger("Hello, world!!");
            Thread t = new Thread(m.DisplayMessage);
            t.Start();
        }
        public class Messenger
        {
            string _msg;
            public Messenger(string msg)
            {
                _msg = msg;
            }
            public void DisplayMessage()
            {
                Console.WriteLine(_msg);
            }
        }


    }
}
