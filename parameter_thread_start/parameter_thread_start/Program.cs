﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace parameter_thread_start
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread t = new Thread(DisplayMessage);
            t.Start("Hello, world!!");
        }
        static void DisplayMessage(object stateArg)
        {
            string msg = stateArg as string;
            if(msg!= null)
            {
                Console.WriteLine(msg);
            }
        }
    }
}
