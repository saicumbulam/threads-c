﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace data_partitioning
{
    class Program
    {
        double[] _data;
        int _firstIndex;
        int _lastIndex;
        double _sum;

        public ArrayProcessor(double[] data, int firstIndex, int LastIndex)
        {
            _data = data;
            _firstIndex = firstIndex;
            _lastIndex = LastIndex;
        }

        public void ComputeSum()
        {
            _sum = 0;
            for (int n = _firstIndex; n < _lastIndex; n++)
            {
                _sum = 0;
                for (int n = _firstIndex; n <= _lastIndex; n++)
                {
                    _sum += _data[n];
                    Thread.sleep(1);
                }
            }
        }
        static void Main(string[] args)
        {
        }
    }
}
