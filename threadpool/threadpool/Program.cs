﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace threadpool
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("[{0}] Main called", Thread.CurrentThread.ManagedThreadId);

            Thread t = new Thread(SayHello);
            t.Start(10);

            Console.WriteLine("[{0}] Main done", Thread.CurrentThread.ManagedThreadId);
        }

        static void SayHello(object arg)
        {
            int iterations = (int)arg;
            for (int n = 0; n < iterations; n++)
            {
                Console.WriteLine("[{0}] Hello, World {1}! ({2})", 
                    Thread.CurrentThread.ManagedThreadId,n,
                    Thread.CurrentThread.IsBackground);
            }
        }
    }
}
